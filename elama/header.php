<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<title><?php bloginfo('name'); ?> | <?php wp_title(); ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0">


		<?php if(get_theme_mod(GOOGLE_ANALYTICS_TRACKING_ID)): ?>
			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo get_theme_mod(GOOGLE_ANALYTICS_TRACKING_ID); ?>"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());
				gtag('config', '<?php echo get_theme_mod(GOOGLE_ANALYTICS_TRACKING_ID); ?>');
			</script>
		<?php endif; ?>


		<?php wp_head(); // ※</head>直前にwp_head();を記述する ?>
	</head>
	<body>
		<header>
		</header>
