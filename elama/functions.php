<?php
/////////////////////////////////////////
// 不要な<head>記述を削除
/////////////////////////////////////////
// rel="prev"とrel=“next"表示の削除
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

// WordPressバージョン表示の削除
remove_action('wp_head', 'wp_generator');

// 絵文字表示のための記述削除（絵文字を使用しないとき）
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// アイキャッチ画像の有効化
add_theme_support('post-thumbnails');









/////////////////////////////////////////
// ログイン画面のカスタマイズ
/////////////////////////////////////////
function my_login_style() {
	wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/assets/css/login.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_style' );








/////////////////////////////////////////
// 404の時、ホームへリダイレクト
/////////////////////////////////////////
function is404_redirect_home() {
  if( is_404() ){
    wp_safe_redirect( home_url( '/' ) );
    exit();
  }
}
add_action( 'template_redirect', 'is404_redirect_home' );






/////////////////////////////////////////
// css・js の読み込み
/////////////////////////////////////////
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	//CSSの読み込みはここから
	//全てのページにassets/css/normalize.cssを読み込み
	wp_enqueue_style( 'main-style', get_template_directory_uri() . '/assets/css/style.css' );

	//JavaScriptの読み込みはここから
	//全てのページにassets/js/functions.jsを読み込み
	wp_enqueue_script('assets-functions', get_template_directory_uri().'/assets/js/main.js');
}






/////////////////////////////////////////
// メニューの有効化
/////////////////////////////////////////
function twpp_setup_theme() {
	register_nav_menus( array(
		'header-global-navigation' => 'Header Global'
	));
}
add_action( 'after_setup_theme', 'twpp_setup_theme' );







/////////////////////////////////////////
// LazyBlocks で自動Wrapされるdivを取り除く
/////////////////////////////////////////
//add_filter( 'lazyblock/block-name/frontend_allow_wrapper', '__return_false' );







/////////////////////////////////////////
// Gutenberg サポートの追加
/////////////////////////////////////////
function gutenberg_support_setup() {

  //Gutenberg用スタイルの読み込み
  add_theme_support( 'wp-block-styles' );

}
add_action( 'after_setup_theme', 'gutenberg_support_setup' );

// 外部ファイルとして読み込む
function add_block_editor_style() {
  wp_enqueue_style('block-editor-style', get_theme_file_uri( 'assets/css/style.css'));
}
add_action( 'enqueue_block_editor_assets', 'add_block_editor_style' );









// GoogleAnalytics設定
define('GOOGLE_ANALYTICS_SECTION', 'google_analytics_section'); //セクションIDの定数化
define('GOOGLE_ANALYTICS_TRACKING_ID', 'google_analytics_tracking_id'); //セッティングIDの定数化
function theme_customizer_googleanalytics( $wp_customize ) {
	$wp_customize->add_section( GOOGLE_ANALYTICS_SECTION , array(
		'title' => 'Google Analytics', //セクション名
		'priority' => 1010, //カスタマイザー項目の表示順
		'description' => 'Google Analytics のトラッキングIDを入力すると、アクセス解析が設定できます。', //セクションの説明
	));

	$wp_customize->add_setting( GOOGLE_ANALYTICS_TRACKING_ID );
	$wp_customize->add_control( GOOGLE_ANALYTICS_TRACKING_ID . '_text', array(
		'section' => GOOGLE_ANALYTICS_SECTION, //セクションID
		'settings' => GOOGLE_ANALYTICS_TRACKING_ID, //セッティングID
		'type' => 'text'
	));
}
add_action( 'customize_register', 'theme_customizer_googleanalytics' );//カスタマイザーに登録
?>
